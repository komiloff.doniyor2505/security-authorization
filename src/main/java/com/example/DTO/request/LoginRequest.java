/**
 * Author: komiloff_doniyor2505@gmail.com
 * Date:4/1/2023
 * Time:12:00 PM
 * Project Name:based-authorization
 */
package com.example.DTO.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String email;
    private String password;
}
