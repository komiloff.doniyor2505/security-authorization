/**
 * Author: komiloff_doniyor2505@gmail.com
 * Date:3/31/2023
 * Time:3:23 PM
 * Project Name:based-authorization
 */
package com.example.DTO.request;

import lombok.Data;

@Data
public class RegisterRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
