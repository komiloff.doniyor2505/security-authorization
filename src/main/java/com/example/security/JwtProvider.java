/**
 * Author: komiloff_doniyor2505@gmail.com
 * Date:3/31/2023
 * Time:3:19 PM
 * Project Name:based-authorization
 */
package com.example.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    @Value("${expiry.date}")
    private Long expiryMilliSeconds;

    @Value("${secret.key}")
    private String secretKey;

    public String generateToken(User user) {
        return generateTokenWithUsername(user.getUsername());
    }

    private String generateTokenWithUsername(String username) {
        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expiryMilliSeconds))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
        return token;
    }

    public String getEmailFromToken(String token) {
        String email = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return email;
    }
}
