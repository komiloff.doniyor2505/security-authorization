/**
 * Author: komiloff_doniyor2505@gmail.com
 * Date:3/31/2023
 * Time:3:21 PM
 * Project Name:based-authorization
 */
package com.example.service;

import com.example.DTO.request.ApiResponse;
import com.example.DTO.request.LoginRequest;
import com.example.DTO.request.RegisterRequest;
import com.example.entity.User;
import com.example.repository.RoleRepository;
import com.example.repository.UserRepository;
import com.example.security.JwtProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    public ApiResponse register(RegisterRequest request) {
        if (userRepository.existsByEmail(request.getEmail()))
            return new ApiResponse("Bunday user mavjud", false);
        User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(List.of(roleRepository.findByName("ROLE_USER")));
        userRepository.save(user);
        return new ApiResponse("User saved", true);
    }

    public ApiResponse login(LoginRequest request) {
        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        String generateToken =
                jwtProvider.generateToken((org.springframework.security.core.userdetails.User) authenticate.getPrincipal());
        return new ApiResponse(generateToken, true);
    }
}
